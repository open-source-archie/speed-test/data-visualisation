# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import dcc
from dash import html
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go
from repo import PostgresRepo
from models import SpeedResult

from dash.dependencies import Input, Output

# Fetch database table
query = PostgresRepo.query_database(model=SpeedResult)

# Convert query object to df
metrics: pd.Dataframe = pd.read_sql(query.statement, query.session.bind)

# Drop irrelevant columns
metrics = metrics.drop(
    [
        "bytes_received",
        "bytes_sent",
        "server_url",
        "server_name",
        "server_country",
        "server_sponsor",
        "server_host",
        "client_isp",
        "client_isprating",
    ],
    1,
)

# Convert from bytes to megabytes
metrics["download"] = metrics["download"].apply(lambda a: a / 1024 / 1024)
metrics["upload"] = metrics["upload"].apply(lambda a: a / 1024 / 1024)


if __name__ == '__main__':
    app.run_server(debug=True)

