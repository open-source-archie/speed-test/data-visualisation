from models import SpeedResult
import pandas as pd
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from datetime import datetime


class MainService:
    def __init__(self, repo):
        self.pg_repo = repo

    def run_service(self):
        # Fetch database table
        query = self.pg_repo.query_database(SpeedResult)

        # Convert query object to df
        metrics: pd.Dataframe = pd.read_sql(query.statement, query.session.bind)

        # Drop irrelevant columns
        metrics = metrics.drop(
            [
                "id",
                "bytes_received",
                "bytes_sent",
                "server_url",
                "server_name",
                "server_country",
                "server_sponsor",
                "server_host",
                "client_isp",
                "client_isprating",
            ],
            1,
        )

        # Convert from bytes to megabytes
        metrics["download"] = metrics["download"].apply(lambda a: a / 1024 / 1024)
        metrics["upload"] = metrics["upload"].apply(lambda a: a / 1024 / 1024)

        metrics_group_time = metrics.groupby(metrics.timestamp.dt.hour).mean()

        # Make plots
        fig = make_subplots(
            rows=2,
            cols=2,
            specs=[
                [{"secondary_y": True}, {"secondary_y": True}],
                [{"secondary_y": False}, {"secondary_y": False}],
            ],
        )

        # RT Download
        fig.add_trace(
            go.Scatter(x=metrics.timestamp, y=metrics.download, name="Download Speed"),
            row=1,
            col=1,
        )
        # Average Download
        fig.add_trace(
            go.Bar(
                x=metrics_group_time.index,
                y=metrics_group_time.download,
                name="Download Speed",
            ),
            row=1,
            col=2,
        )
        # Average Upload
        fig.add_trace(
            go.Bar(
                x=metrics_group_time.index,
                y=metrics_group_time.upload,
                name="Upload Speed",
            ),
            row=1,
            col=2,
        )
        # Average ping
        fig.add_trace(
            go.Scatter(
                x=metrics_group_time.index,
                y=metrics_group_time.ping,
                name="Average Ping",
            ),
            secondary_y=True,
            row=1,
            col=2,
        )
        # RT Upload
        fig.add_trace(
            go.Scatter(x=metrics.timestamp, y=metrics.upload, name="Upload Speed"),
            row=1,
            col=1,
        )
        # RT Ping
        fig.add_trace(
            go.Scatter(x=metrics.timestamp, y=metrics.ping, name="Ping"),
            secondary_y=False,
            row=2,
            col=1,
        )

        # Update xaxis properties
        fig.update_xaxes(title_text="Time", row=1, col=1)
        fig.update_xaxes(title_text="Time", row=2, col=1)
        fig.update_xaxes(title_text="Hour of the day", row=1, col=2)

        # Update yaxis properties
        fig.update_yaxes(title_text="Bandwidth (MB/s)", row=1, col=1)
        fig.update_yaxes(title_text="Latency (ms)", row=1, col=2, secondary_y=True, range=[0, 130])
        fig.update_yaxes(title_text="Bandwidth (MB/s)", row=1, col=2, secondary_y=False, range=[0, 80])

        # Update title
        fig.update_layout(title_text="Internet Metrics")
        fig.show()
