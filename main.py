import logging
from dotenv import load_dotenv

from repo import PostgresRepo
from service import MainService


def main():
    # Setup logging
    logging.basicConfig(
        level=logging.WARNING, format="%(asctime)s:%(name)s:%(levelname)s:%(message)s"
    )

    # Load config
    load_dotenv()

    # Construct repo
    repo = PostgresRepo()

    # Construct service
    main_service = MainService(repo)

    main_service.run_service()


if __name__ == "__main__":
    main()
